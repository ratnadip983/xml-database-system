# README #

---------------------XML Database System----------------------------

### What is this repository for? ###

* This Relational database system is designed to store database as separate XML file. 
* This is uploaded after completion. So, this is most latest version
* You can read the paper (https://www.researchgate.net/publication/277009600_Design_and_Implementation_of_a_Relational_Database_Based_on_XML) to batter understand the project.

### How do I get set up? ###

* It's an .Net application
* .Net 4.5 is required (Some functions are used those are not supported by earlier version of .Net)
* Visual Studio 2012
* Windows Visual PowerPack must be integrate with visual studio which is eliminated from visual studio 2013 and later version
* Just Run the project using visual studio


### Who do I talk to? ###

* Ratnadip Kuri
* Email: ratnadipk983@gmail.com